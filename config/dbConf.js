require('dotenv').config({ path: __dirname + "/env" });
const mysql = require("mysql");

const env = process.env;

// Create a connection to the database
const connection = mysql.createConnection({
    host: env.HOST,
    user: env.USER,
    password: env.PASSWORD,
    database: env.DB
});

// open the MySQL connection
connection.connect(error => {
    if (error) throw error;
    console.log("Successfully connected to the database.");
});

module.exports = connection;