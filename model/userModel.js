const sql = require('../api-atrio/config/dbConf');

class User {
    constructor(username) {
        this.username = username;
    }

    create = (newUser, result) => {
        sql.query("INSERT INTO users SET ?", newUser, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }

            console.log("created customer: ", { id: res.id, ...newUser });
            result(null, { id: res.id, ...newUser });
        });
    };
}

module.exports = User;