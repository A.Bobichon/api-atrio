const sql = require('../api-atrio/config/dbConf');

class Post {
    constructor(title, content, createdDate, userId) {
        this.title = title;
        this.content = content;
        this.createdDate = new Date();
        this.userId = userId;
    }

    // Get one post
    getOne = (id, result) => {
        sql.query(`SELECT * FROM Posts WHERE id = ${id}`, (err, res) => {
            if (err) return console.log("error: ", err);
        });

        if (res.length) return result(err, null);
    }

    create = (newPost, result) => {
        sql.query("INSERT INTO Posts SET ?", newPost, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }

            console.log("created customer: ", { id: res.id, ...newPost });
            result(null, { id: res.id, ...newPost });
        });
    };

    remove = (id, result) => {
        sql.query("DELETE FROM customers WHERE id = ?", id, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found Customer with the id
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("deleted customer with id: ", id);
            result(null, res);
        });
    };
}

module.exports = Post;