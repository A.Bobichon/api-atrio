const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const dbConf = require('../api-atrio/config/dbConf');

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

// Set port
app.listen(8080, () => {
    console.log('Hello world!!');
});