const userController = require('../controller/userController');
const postController = require('../controller/postController');


module.exports = app => {

    //User path
    app.post("/user/create", userController.create);

    //Post path
    app.post("/post/create", postController.create);
    app.delete("/post/delete", postController.delete);
    app.get("/post/getOne/:id", postController.getOne);
}